"use strict";

const Joi = require('joi');

class Validator {
    constructor () {}

    validateSchema (data, schema) {
        return new Promise((resolve, reject) => {
            let errors = [];
            const valids = [];

            if (data.hasOwnProperty('params') && schema.hasOwnProperty('params')) {
                Joi.validate(data.params, schema.params, (error, value) => {
                    if (error) {
                        errors = error.details.map(entry => entry);
                    } else {
                        valids.push(value);
                    }
                })
            }

            if (data.hasOwnProperty('query') && schema.hasOwnProperty('query')) {
                Joi.validate(data.query, schema.query, (error, value) => {
                    if (error) {
                        errors = error.details.map(entry => entry);
                    } else {
                        valids.push(value);
                    }
                })
            }

            if (data.hasOwnProperty('body') && schema.hasOwnProperty('body')) {
                Joi.validate(data.body, schema.body, (error, value) => {
                    if (error) {
                        errors = error.details.map(entry => entry);
                    } else {
                        valids.push(value);
                    }
                })
            }

            if (errors.length > 0) {
                return reject(errors);
            }

            return resolve(valids);
        });
    }

    validateEntry (data, entry, schemaType) {
        const schema = this.getSchema(entry, schemaType);
        return new Promise((resolve, reject) => {
            return Joi.validate(data, schema, (error, value) => {
                if (error) {
                    reject(error);
                }
                resolve(value);
            });
        });
    }

    formatError (errors) {
        const messages = errors.map(error => error.message);
        return messages.join(', ');
    }

    getSchema (entry, schemaType) {
        const schema = {};
        switch (schemaType) {
            case 'string':
                schema[entry] = Joi.string().required();
                break;
            case 'number':
                schema[entry] = Joi.number().required().positive();
                break;
            case 'boolean':
                schema[entry] = Joi.boolean().required();
                break;
            default:
                schema[entry] = Joi.any().required();
                break;
        }
        return schema;
    }
}

module.exports = Validator;
