"use strict";

const socket = require('./ServiceLocator').invoke('socket');


class BaseService {

    constructor (validator, cache, logger) {
        this.validator = validator;
        this.cache = cache;
        this.logger = logger;
        this.model = undefined;
        this.socket = socket;
        this.limit = 10;
        this.page = 1;
    }

    getLimit () {
        return this.limit;
    }

    getPage () {
        return this.page;
    }

    use (schema) {
        if (!schema) {
            throw new Error ('Schema cannot be null or empty');
        }

        if (typeof schema !== 'function') {
            throw new Error ('Schema type is not valid');
        }

        this.model = schema;
        return this;
    }

    exec (staticMethod, filter) {
        if (!this.model) {
            throw new Error ('Schema cannot be undefined');
        }

        return this.model[staticMethod](filter)
            .then(response => response)
            .catch(error => {
                throw error
            });
    }

    getSkip (page, limit) {
        return limit * (page - 1);
    }

    countDocuments (filter) {
        if (!this.model) {
            throw new Error ('Schema cannot be undefined');
        }

        return this.model.count(filter)
            .then(count => count)
            .catch(error => {
                throw error
            });
    }

    create (payload, markModify) {
        if (!this.model) {
            throw new Error ('Schema cannot be undefined');
        }

        const model = new this.model(payload);

        if (markModify) {
            model.markModified(markModify);
        }

        return model
            .save()
            .then(document => document)
            .catch(error => {
                throw error
            });
    }

    updateOne (filter, payload) {
        if (!this.model) {
            throw new Error ('Schema cannot be undefined');
        }

        return this.model.findOneAndUpdate(filter, payload, { new: true })
            .then(document => document)
            .catch(error => {
                throw error
            });
    }

    upsertOne (filter, payload) {
        if (!this.model) {
            throw new Error ('Schema cannot be undefined');
        }

        return this.model.findOneAndUpdate(filter, payload, { upsert: true })
            .then(document => document)
            .catch(error => {
                throw error
            });
    }

    fetch (filter, populate, query) {
        if (!this.model) {
            throw new Error ('Schema cannot be undefined');
        }

        let { page, limit, select } = query;
        page = Math.ceil(parseInt(page));
        limit = Math.ceil(parseInt(limit));

        let model = this.model.find(filter)
            .skip(this.getSkip(page, limit))
            .limit(limit)
            .sort({ 'created_at': -1 });

        if (select) {
            model.select(select);
        }

        if (populate) {
            model = model.populate(populate);
        }

        return model
            .then(documents => documents)
            .catch(error => {
                throw error
            });
    }

    fetchOne (filter, populate) {
        if (!this.model) {
            throw new Error ('Schema cannot be undefined');
        }

        let model = this.model.findOne(filter);

        if (populate) {
            model = model.populate(populate);
        }

        return model
            .then(document => document)
            .catch(error => {
                throw error
            });
    }

    fetchAll (filter, populate) {
        if (!this.model) {
            throw new Error ('Schema cannot be undefined');
        }

        let model = this.model.find(filter);

        if (populate) {
            model = model.populate(populate);
        }

        return model
            .then(documents => documents)
            .catch(error => {
                throw error
            });
    }

    deleteOne (filter) {
        if (!this.model) {
            throw new Error ('Schema cannot be undefined');
        }

        return this.model.findOneAndDelete(filter)
            .then(document => document)
            .catch(error => {
                throw error
            });
    }

    deleteMany (filter, update, multi) {
        if (!this.model) {
            throw new Error ('Schema cannot be undefined');
        }

        return this.model.update(filter, update, multi)
            .then(documents => documents)
            .catch(error => {
                throw error
            });
    }
}

module.exports = BaseService;
