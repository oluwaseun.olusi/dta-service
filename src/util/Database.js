"use strict";

const mongoose = require('mongoose');
const di = require('./ServiceLocator');

class Database {

    constructor (logger) {
        this.logger = logger;
        this.configuration = di.invoke('configuration');
        this.dbService = mongoose;
        this.service_name = 'Database Service';
    }

    connect () {
        this.dbService
            .connect(this.configuration.database.uri, {
                autoIndex: false,
                useNewUrlParser: true,
                useFindAndModify: false
            }, err => {
            if (err) {
                this.logger.log(`error`, `Failed to connect to DB ${ this.configuration.database.name } with error: ${ err.toString() }`, this.service_name);
            } else {
                this.logger.log(`info`, `Connected to DB ${ this.configuration.database.name }`, this.service_name);
            }
        });
    }
}

module.exports = Database;
