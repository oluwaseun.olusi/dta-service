"use strict";

const SocketIO = require("socket.io");

let Socket = {};

Socket.connect = function (server) {
    Socket.IO = new SocketIO(server);
    Socket.IO.on('connection', function (client) {
        console.log("New Client Connected", client.id);
    });
};

Socket.emit = function (emitData) {
    Socket.IO.emit(emitData.subject, emitData.data);
};

module.exports = Socket;
