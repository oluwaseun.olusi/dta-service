"use strict";

const redis = require('./ServiceLocator').invoke('redis');

class Cache {
    constructor () {
        this.redis = redis.cacheDB;
        this.conversions = {
            years: 31536000,
            year: 31536000,
            months: 2628000,
            month: 2628000,
            weeks: 604800,
            week: 604800,
            days: 86400,
            day: 86400,
            hours: 3600,
            hour: 3600,
            minutes: 60,
            minute: 60,
            seconds: 1,
            second: 1
        };
        this.today = new Date();
    }

    formatData (type, data) {
        if (type === 'read') {
            return JSON.parse(data);
        } else if (type === 'write') {
            return JSON.stringify(data);
        }
    }

    delete (key) {
        return this.redis.del(key)
            .then(() => {
                return true;
            })
            .catch(error => {
                throw error;
            });
    }

    deleteAll (pattern) {
        return this.redis.keys(pattern)
            .then(keys => {
                if (!keys.length) return false;

                return this.redis.del(keys)
                    .then(() => {
                        return true;
                    })
                    .catch(error => {
                        throw error;
                    });
            })
            .catch(error => {
                throw error;
            });
    }

    write (key, data, TTL) {
        return this.redis.exists(key)
            .then(redisResponse => {
                if (redisResponse) return this.update(key, data, TTL);

                return this.redis.set(key, this.formatData('write', data))
                    .then(async () => {
                        const ttl = await this.setTTL(key, TTL);
                        return data;
                    })
                    .catch(error => {
                        throw error;
                    });
            })
            .catch(error => {
                throw error;
            });
    }

    increase (key) {
        return this.redis.exists(key)
            .then(redisResponse => {
                if (!redisResponse) return this.write(key, 1);

                return this.redis.incr(key)
                    .then(count => {
                        return count;
                    })
                    .catch(error => {
                        throw error;
                    });
            })
            .catch(error => {
                throw error;
            });
    }

    decrease (key) {
        return this.redis.exists(key)
            .then(redisResponse => {
                if (!redisResponse) return this.write(key, 0);

                return this.redis.decr(key)
                    .then(count => {
                        return count;
                    })
                    .catch(error => {
                        throw error;
                    });
            })
            .catch(error => {
                throw error;
            });
    }

    read (key) {
        return this.redis.exists(key)
            .then(redisResponse => {
                if (!redisResponse) return null;

                return this.redis.get(key)
                    .then(redisResponse => {
                        return this.formatData('read', redisResponse);
                    })
                    .catch(error => {
                        throw error;
                    });
            })
            .catch(error => {
                throw error;
            });
    }

    readAll (pattern) {
        return this.redis.keys(pattern)
            .then(async keys => {
                let redisData = [];
                for (let k = 0; k < keys.length; k++) {
                    const data = await this.asyncRead(keys[k]);
                    if (data) redisData.push(data);
                }
                return redisData;
            })
            .catch(error => {
                throw error;
            })
    }

    async asyncRead (key) {
        return this.redis.get(key)
            .then(redisResponse => {
                return this.formatData('read', redisResponse);
            })
            .catch(error => {
                throw error;
            });
    }

    update (key, data, TTL) {
        return this.redis.set(key, this.formatData('write', data))
            .then(async () => {
                const ttl = await this.setTTL(key, TTL);
                return data;
            })
            .catch(error => {
                throw error;
            });
    }

    setTTL (key, TTL) {
        const expireKey = parseInt(this.getExpireKey(TTL || '1 day'));
        return this.redis.expire(key, expireKey)
            .then(redisResponse => {
                return true;
            })
            .catch(error => {
                throw error;
            });
    }

    getExpireKey (TTL) {
        let expireKey = 0;
        let value = 1;
        let duration = '';
        TTL = TTL.split(',');

        for (let i = 0; i < TTL.length; i++) {
            let ttl = TTL[i];
            ttl = ttl.trim().split(' ');

            if (ttl.length > 1) {
                value = ttl[0];
                duration = ttl[1];
            } else {
                duration = ttl[0];
            }

            if (this.conversions.hasOwnProperty(duration)) {
                expireKey += parseInt(value) * this.conversions[duration];
            }
        }

        return parseInt(expireKey);
    }
}

module.exports = Cache;
