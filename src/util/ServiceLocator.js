"use strict";

let ServiceLocator = {};

let serviceRepository = {};
let serviceCache = {};

ServiceLocator.register = function (service, constructor) {
    if (!service || service === '') {
        throw new Error ('Trying to register an empty or invalid service name');
    }

    if (typeof constructor !== 'function') {
        throw new Error ('Service constructor is not a function');
    }

    if (!serviceRepository.hasOwnProperty(service)) {
        serviceRepository[service] = constructor;
    }
};

ServiceLocator.invoke = function (service) {
    if (!service || service === '') {
        throw new Error('Cannot invoke an empty or invalid service name');
    }

    if (!serviceRepository.hasOwnProperty(service)) {
        throw new Error(`Trying to invoke an unregistered service: ${ service }`);
    }

    if (typeof serviceRepository[service] !== 'function') {
        throw new Error('Service constructor is not a function');
    }

    if (!serviceCache.hasOwnProperty(service)) {
        const serviceConstructor = serviceRepository[service];
        const newService = serviceConstructor.call();
        if (newService) {
            serviceCache[service] = newService;
        }
    }

    return serviceCache[service];
};

ServiceLocator.bulkInvoke = function (serviceList) {
    if (!serviceList.length) {
        throw new Error('Cannot call bulk invoke with empty service list');
    }

    let services = {};

    serviceList.map(service => {
        services[service] = ServiceLocator.invoke(service);
    });

    return services;
};

ServiceLocator.register('configuration', function () {
    return require('./Configuration');

});

ServiceLocator.register('database', function () {
    const Database = require('./Database');
    const logger = ServiceLocator.invoke('logger');
    return new Database(logger);
});

ServiceLocator.register('redis', function () {
    const Redis = require('./Redis');
    return new Redis();
});

ServiceLocator.register('cache', function () {
    const Cache = require('./Cache');
    return new Cache();
});

ServiceLocator.register('middleware', function () {
    const Middleware = require('./Middleware');
    return new Middleware();
});

ServiceLocator.register('validator', function () {
    const Validator = require('./Validator');
    return new Validator();
});

ServiceLocator.register('socket', function () {
    const Socket = require('./Socket');
    return Socket;
});

ServiceLocator.register('logger', function () {
    const Logger = require('./Logger');
    return new Logger();
});

ServiceLocator.register('uploader', function () {
    const Uploader = require('./Uploader');
    return new Uploader();
});

ServiceLocator.register('base', function () {
    const BaseService = require('./BaseService');
    const { validator, cache, logger } = ServiceLocator.bulkInvoke([ 'validator', 'cache', 'logger' ]);
    return new BaseService(validator, cache, logger);
});

ServiceLocator.register('product', function () {
    const Service = require('../service/Product');
    const { validator, cache, logger } = ServiceLocator.bulkInvoke([ 'validator', 'cache', 'logger' ]);
    return new Service(validator, cache, logger);
});

ServiceLocator.register('category', function () {
    const Service = require('../service/Category');
    const { validator, cache, logger } = ServiceLocator.bulkInvoke([ 'validator', 'cache', 'logger' ]);
    return new Service(validator, cache, logger);
});

module.exports = ServiceLocator;
