"use strict";

const { Router } = require('express');

const product = require('../route/Product');
const category = require('../route/Category');

const router = Router();

router.get('/ping', (request, response) => {
    response.status(200).send({
        message: 'Service is Up',
    });
});


router.use(product);
router.use(category);

module.exports = router;
