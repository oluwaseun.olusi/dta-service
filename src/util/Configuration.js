"use strict";

const configuration = {
    staging: {
        database: {
            host: process.env.DATABASE_HOST,
            port: process.env.DATABASE_PORT,
            name: process.env.DATABASE_NAME,
            uri: process.env.MONGODB_URI,
            pool: process.env.DATABASE_POOL,
        },
        server: {
            port: process.env.PORT,
            base_url: process.env.API_BASE
        },
        redis: {
            host: process.env.REDIS_HOST,
            port: process.env.REDIS_PORT,
            password: process.env.REDIS_PASSWORD,
            batch_ttl: process.env.ITEM_TTL || 300,
            list_ttl: process.env.LIST_TTL || 30,
            url: process.env.REDIS_URL
        },
        cache: {
            database: process.env.CACHE_DATABASE || '1'
        },
        cloudinary: {
            use_cloudinary: process.env.USE_CLOUDINARY,
            uri: process.env.CLOUDINARY_URL
        }
    },
    development: {
        database: {
            host: process.env.DEV_DATABASE_HOST,
            port: process.env.DEV_DATABASE_PORT,
            name: process.env.DEV_DATABASE_NAME,
            uri: process.env.DEV_MONGODB_URI,
            pool: process.env.DEV_DATABASE_POOL
        },
        server: {
            port: process.env.API_PORT,
            base_url: process.env.API_BASE
        },
        redis: {
            host: process.env.DEV_REDIS_HOST,
            port: process.env.DEV_REDIS_PORT,
            password: process.env.DEV_REDIS_PASSWORD,
            batch_ttl: process.env.ITEM_TTL || 300,
            list_ttl: process.env.LIST_TTL || 30,
            url: process.env.DEV_REDIS_URL
        },
        cache: {
            database: process.env.DEV_CACHE_DATABASE || '1'
        },
        cloudinary: {
            use_cloudinary: process.env.USE_CLOUDINARY,
            uri: process.env.CLOUDINARY_URL
        }
    }
};

module.exports = configuration[process.env.NODE_ENV];
