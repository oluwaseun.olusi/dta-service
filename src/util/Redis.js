"use strict";

const redis = require('ioredis');
const di = require('./ServiceLocator');

const Redis = function () {
    const config = di.invoke('configuration');

    const cacheDB = new redis(config.redis.url, {
        port: parseInt(config.redis.port),
        host: config.redis.host,
        family: 4,
        password: config.redis.password,
        db: parseInt(config.cache.database)
    });

    return { cacheDB };
};

module.exports = Redis;
