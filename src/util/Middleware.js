"use strict";

const shortid = require('shortid');
const di = require('./ServiceLocator');

class Middleware {

    constructor () {
        const { configuration, logger } = di.bulkInvoke([ 'configuration', 'logger' ]);
        this.logger = logger;
        this.configuration = configuration;
        this.service_name = 'Midlleware Service';
    }

    slugify (title) {
        return (request, response, next) => {
            request.body.slug = request.body[title].toString().toLowerCase()
                .replace(/\s+/g, '-')
                .replace(/[^\w\-]+/g, '')
                .replace(/\-\-+/g, '-')
                .replace(/^-+/, '')
                .replace(/-+$/, '');
            next();
            return null;
        };
    }

}

module.exports = Middleware;
