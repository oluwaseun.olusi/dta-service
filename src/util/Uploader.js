"use strict";

const multer = require('multer');
const cloudinary = require('cloudinary');
const configuration = require('./ServiceLocator').invoke('configuration');

class Uploader {

    constructor () {
        this.multer = multer;
        this.cloudinary = cloudinary;
        /*this.cloudinary.config({
            cloud_name: configuration.cloudinary.cloud_name,
            api_key: configuration.cloudinary.api_key,
            api_secret: configuration.cloudinary.api_secret
        });*/

        this.storage = this.multer.diskStorage({
            destination: function (request, file, callback) {
                callback(null, './public')
            },
            filename: function (request, file, callback) {
                callback(null, `${ Date.now() }-${ file.originalname}`);
            }
        });

        this.image_filter = function (request, file, callback) {
            if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/i)) {
                return callback(new Error('Only image files are allowed!'), false);
            }
            callback(null, true);
        };
    }

    local_upload () {
        return this.multer({ storage: this.storage, fileFilter: this.image_filter });
    }

    cloud_upload () {
        return (request, response, next) => {
            if (request.files) {
                const { files } = request;
                return Promise.all(files.map(file => this.handle_cloud_upload(file)))
                    .then(cloud_response => {
                        try {
                            let media = [];
                            cloud_response.map(entry => {
                                if (entry.secure_url) {
                                    media.push(
                                        { media_url: entry.secure_url }
                                    );
                                }
                            });
                            request.body.media = media;
                            next();
                            return null;
                        } catch (error) {
                            throw error;
                        }
                    }).catch(error => next(error));
            } else {
                next();
                return null;
            }
        };
    }

    handle_cloud_upload (file) {
        return this.cloudinary.v2.uploader.upload(file.path, (error, cloudinary_response) => {
            if (error) {
                return null;
            }

            return cloudinary_response;
        });
    }
}

module.exports = Uploader;
