"use strict";

const { createLogger, format, transports, config } = require('winston');
const { combine, colorize, simple } = format;

class Logger {

    constructor () {
        this.logger = createLogger({
            levels: config.npm.levels,
            format: combine(colorize(), simple()),
            transports: [
                new transports.Console()
            ]
        });
    }

    log (type, message, service) {
        message = `${ service || 'Global' } - ${ message }`;
        this.logger.log(type, message);
    }

}

module.exports = Logger;
