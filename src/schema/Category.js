"use strict";

const { Schema, model } = require('mongoose');
const { ObjectId } = Schema.Types;

const Category = new Schema({
    category_name: { type: String, required: true },
    slug: { type: String, required: true },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now }
});

Category.statics = {
    available (filter) {
        return this.findOne(filter)
            .then(document => document)
            .catch(error => error);
    },
    duplicate (filter) {
        return this.findOne(filter)
            .then(document => document)
            .catch(error => error);
    }
};

module.exports = model("Category", Category);
