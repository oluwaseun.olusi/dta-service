"use strict";

const { Schema, model } = require('mongoose');
const { ObjectId } = Schema.Types;

const Product = new Schema({
    category_id: { type: ObjectId, required: true, ref: 'Category' },
    name: { type: String, required: true },
    slug: { type: String, required: true },
    description: { type: String, required: true },
    price: { type: Number, required: true },
    featured_media: {
        media_url: { type: String, required: true },
    },
    media: [
        {
            media_url: { type: String, required: true },
        }
    ],
    tag: [
        { type: String, required: false }
    ],
    color: [
        { type: String, required: false }
    ],
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now }
});

Product.statics = {
    available (filter) {
        return this.findOne(filter)
            .then(document => document)
            .catch(error => error);
    },
    duplicate (filter) {
        return this.findOne(filter)
            .then(document => document)
            .catch(error => error);
    }
};

module.exports = model("Product", Product);
