"use strict";

const Joi = require('joi');

module.exports = {
    createCategory: function () {
        return {
            body: {
                category_name: Joi.string().required(),
                slug: Joi.string().required()
            }
        };
    }
};
