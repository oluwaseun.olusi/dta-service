"use strict";

const Joi = require('joi');

module.exports = {
    createProduct: function () {
        return {
            body: {
                category_id: Joi.string().required(),
                name: Joi.string().required(),
                slug: Joi.string().required(),
                description: Joi.string().required(),
                price: Joi.number().required(),
                featured_media: Joi.any().required(),
                media: Joi.array().items(Joi.any()).min(1).required(),
                tag: Joi.array().items(Joi.string()).min(1).required(),
                color: Joi.array().items(Joi.string()).min(1).required()
            }
        };
    },
    getProduct: function () {
        return {
            params: {
                _id: Joi.string().required()
            }
        };
    }
};
