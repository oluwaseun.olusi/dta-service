"use strict";

const lodash = require('lodash');
const ProductSchema = require('../schema/Product');
const BaseService = require('../util/BaseService');
const schema = require('../validation/Product');


class Product extends BaseService {

    constructor (validator, cache, logger) {
        super(validator, cache, logger);
        this.requiredFields = [
            'category_id', 'name', 'slug', 'description', 'price', 'featured_media', 'media', 'tag', 'color', 'created_at', 'updated_at'
        ];
        this.service_name = 'Product Service';
        this.cache_key = 'products';
        this.schema = ProductSchema;
    }

    available () {
        return (request, response, next) => {
            this.logger.log('info', `available()`, this.service_name);
            const { product_id } = request.body;

            this.logger.log('info', `Check if product document exists, ${ product_id }`, this.service_name);
            return this.use(this.schema).exec('available', { _id: product_id, "deleted.is_deleted": false })
                .then(document => {
                    if (!document) {
                        this.logger.log('info', `Product document not found`, this.service_name);
                        return response.status(400).send({
                            status: 'error',
                            message: 'Product does not exist'
                        });
                    }

                    this.logger.log('info', `Product document found, calling next function`, this.service_name);
                    next();
                    return null;
                })
                .catch(error => {
                    this.logger.log('error', `Reading from database error, ${ error.toString() }`, this.service_name);
                    return next(error);
                });
        };
    }

    duplicate () {
        return (request, response, next) => {
            this.logger.log('info', `duplicate()`, this.service_name);
            const { slug } = request.body;

            this.logger.log('info', `Check for duplicate product document`, this.service_name);
            return this.use(this.schema).exec('duplicate', { slug })
                .then(news => {
                    if (news) {
                        this.logger.log('info', `Duplicate product document found`, this.service_name);
                        return response.status(400).send({
                            status: 'error',
                            message: 'Product already exist'
                        });
                    }

                    this.logger.log('info', `Product document is unique, callind next function`, this.service_name);
                    next();
                    return null;
                })
                .catch(error => {
                    this.logger.log('error', `Reading from database error, ${ error.toString() }`, this.service_name);
                    return next(error);
                });
        };
    }

    createProduct () {
        return (request, response, next) => {
            this.logger.log('info', `createProduct()`, this.service_name);

            let body = lodash.pick(request.body, this.requiredFields);
            body.featured_media = body.media[0];

            this.logger.log('info', `Validating product document payload ${ JSON.stringify(body) }`, this.service_name);
            return this.validator.validateSchema({ body }, schema.createProduct())
                .then(async () => {
                    try {
                        if (!body.created_at) body.created_at = new Date();
                        if (!body.updated_at) body.updated_at = new Date();

                        this.logger.log('info', `Creating product document with payload ${ JSON.stringify(body) }`, this.service_name);

                        const document = await this.use(this.schema).create(body);
                        this.logger.log('info', `Purging ${ this.cache_key }:all:* cache`, this.service_name);
                        this.cache.deleteAll(`${ this.cache_key }:all:*`);

                        this.logger.log('info', `Sending data, ${ document }`, this.service_name);

                        this.socket.emit({ subject: 'Data', data: { type: 'Create', repository: 'Products', content: document } });

                        return response.status(201).send({
                            status: 'success',
                            message: 'Product created successfully',
                            data: document
                        });
                    } catch (error) {
                        this.logger.log('error', `Saving to database error, ${ error.toString() }`, this.service_name);
                        return next(error);
                    }
                })
                .catch(error => {
                    this.logger.log('error', `Payload validation error, ${ error.toString() }`, this.service_name);
                    const message = this.validator.formatError(error);
                    return next({ message, statusCode: 400 });
                });
        };
    }

    getAllProduct () {
        return (request, response, next) => {
            this.logger.log('info', `getAllProduct()`, this.service_name);

            const { page, limit } = request.query;

            const query = {
                page: (page || this.getPage()),
                limit: (limit || this.getLimit()),
                select: {
                    "_id": 1,
                    "name": 1,
                    "price": 1
                }
            };

            this.logger.log('info', `Reading cache ${ this.cache_key }:all:${ JSON.stringify(query) }`, this.service_name);
            return this.cache.read(`${ this.cache_key }:all:${ JSON.stringify(query) }`)
                .then(async cacheData => {
                    if (cacheData) {
                        this.logger.log('info', `Cache data found, ${ JSON.stringify(cacheData) }`, this.service_name);
                        return response.status(200).send({
                            status: 'success',
                            message: 'All products retrieved successfully',
                            data: cacheData,
                            meta: request.meta
                        });
                    }

                    this.logger.log('info', `No cache data found, reading database data`, this.service_name);
                    try {
                        const documents = await this.use(this.schema).fetch({ }, null, query);

                        if (documents && documents.length > 0) {
                            this.logger.log('info', `Writing data to cache ${ this.cache_key }:all:${ JSON.stringify(query) }`, this.service_name);
                            this.cache.write(`${ this.cache_key }:all:${ JSON.stringify(query) }`, documents);
                        }

                        this.logger.log('info', `Sending data, ${ JSON.stringify(documents) }`, this.service_name);
                        return response.status(200).send({
                            status: 'success',
                            message: 'All products retrieved successfully',
                            data: documents,
                            meta: request.meta
                        });
                    } catch (error) {
                        this.logger.log('error', `Reading from database error, ${ error.toString() }`, this.service_name);
                        return  next(error);
                    }
                })
                .catch(error => {
                    this.logger.log('error', `Reading from cache error, ${ error.toString() }`, this.service_name);
                    return next(error);
                });
        };
    }

    getProduct () {
        return (request, response, next) => {
            this.logger.log('info', `getProduct()`, this.service_name);
            const { _id } = request.params;

            this.logger.log('info', `Validating product id ${ _id }`, this.service_name);
            return this.validator.validateSchema({ params: { _id } }, schema.getProduct())
                .then(() => {
                    this.logger.log('info', `Reading cache ${ this.cache_key }:${ _id }`, this.service_name);
                    return this.cache.read(`${ this.cache_key }:${ _id }`)
                        .then(async cacheData => {
                            if (cacheData) {
                                this.logger.log('info', `Cache data found, ${ JSON.stringify(cacheData) }`, this.service_name);
                                return response.status(200).send({
                                    status: 'success',
                                    message: 'Product retrieved successfully',
                                    data: cacheData
                                });
                            }

                            this.logger.log('info', `No cache data found, reading database data`, this.service_name);
                            try {
                                const document = await this.use(this.schema).fetchOne({ _id }, { path: 'category_id' });

                                if (document) {
                                    this.logger.log('info', `Writing data to cache ${ this.cache_key }:${ _id }`, this.service_name);
                                    this.cache.write(`${ this.cache_key }:${ _id }`, document);
                                }

                                this.logger.log('info', `Sending data, ${ JSON.stringify(document) }`, this.service_name);
                                return response.status(200).send({
                                    status: 'success',
                                    message: 'Product retrieved successfully',
                                    data: document
                                });
                            } catch (error) {
                                this.logger.log('error', `Reading from database error, ${ error.toString() }`, this.service_name);
                                return  next(error);
                            }
                        })
                        .catch(error => {
                            this.logger.log('error', `Reading from cache error, ${ error.toString() }`, this.service_name);
                            return next(error);
                        });
                })
                .catch(error => {
                    this.logger.log('error', `Payload validation error, ${ error.toString() }`, this.service_name);
                    const message = this.validator.formatError(error);
                    return next({ message, statusCode: 400 });
                });
        };
    }

    getMeta (filter, extra_params) {
        return async (request, response, next) => {

            if (extra_params && typeof extra_params === 'object') {
                extra_params.map(param => {
                    if (typeof param === 'string') {
                        filter[param] = request.params[param];
                    }

                    if (typeof param === 'object') {
                        Object.keys(param).map(key => {
                            filter[key] = request.params[param[key]];
                        });
                    }
                });
            }

            this.logger.log('info', `getMeta(${ JSON.stringify(filter) })`, this.service_name);

            let { page, limit } = request.query;
            limit = (Math.ceil(parseInt(limit)) || this.getLimit());
            page = (Math.ceil(parseInt(page)) || this.getPage());

            try {
                const count = await this.use(this.schema).countDocuments(filter);
                request.meta = {
                    totalPage: Math.ceil(count / limit),
                    perPage: limit,
                    currentPage: page,
                    totalDocument: count
                };
                next();
                return null;
            } catch (error) {
                this.logger.log('error', `Error building response meta, ${ error.toString() }`, this.service_name);
                return next(error);
            }
        };
    }

}

module.exports = Product;
