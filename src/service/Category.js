"use strict";

const lodash = require('lodash');
const CategorySchema = require('../schema/Category');
const BaseService = require('../util/BaseService');
const schema = require('../validation/Category');


class Category extends BaseService {

    constructor (validator, cache, logger) {
        super(validator, cache, logger);
        this.requiredFields = [
            'category_name', 'slug', 'created_at', 'updated_at'
        ];
        this.service_name = 'Category Service';
        this.cache_key = 'categories';
        this.schema = CategorySchema;
    }

    available () {
        return (request, response, next) => {
            this.logger.log('info', `available()`, this.service_name);
            const { category_id } = request.body;

            this.logger.log('info', `Check if category document exists`, this.service_name);
            return this.use(this.schema).exec('available', { _id: category_id })
                .then(document => {
                    if (!document) {
                        this.logger.log('info', `Category document not found`, this.service_name);
                        return response.status(400).send({
                            status: 'error',
                            message: 'Category does not exist'
                        });
                    }

                    this.logger.log('info', `Category document found, calling next function`, this.service_name);
                    next();
                    return null;
                })
                .catch(error => {
                    this.logger.log('error', `Reading from database error, ${ error.toString() }`, this.service_name);
                    return next(error);
                });
        };
    }

    duplicate () {
        return (request, response, next) => {
            this.logger.log('info', `duplicate()`, this.service_name);
            const { slug } = request.body;

            this.logger.log('info', `Check for duplicate category document`, this.service_name);
            return this.use(this.schema).exec('duplicate', { slug })
                .then(news => {
                    if (news) {
                        this.logger.log('info', `Duplicate category document found`, this.service_name);
                        return response.status(400).send({
                            status: 'error',
                            message: 'Category already exist'
                        });
                    }

                    this.logger.log('info', `Category document is unique, calling next function`, this.service_name);
                    next();
                    return null;
                })
                .catch(error => {
                    this.logger.log('error', `Reading from database error, ${ error.toString() }`, this.service_name);
                    return next(error);
                });
        };
    }

    createCategory () {
        return (request, response, next) => {
            this.logger.log('info', `createNewsCategory()`, this.service_name);

            let body = lodash.pick(request.body, this.requiredFields);

            this.logger.log('info', `Validating category document payload ${ JSON.stringify(body) }`, this.service_name);
            return this.validator.validateSchema({ body }, schema.createCategory())
                .then(async () => {
                    try {
                        if (!body.created_at) body.created_at = new Date();
                        if (!body.updated_at) body.updated_at = new Date();

                        this.logger.log('info', `Creating category document with payload ${ JSON.stringify(body) }`, this.service_name);
                        const document = await this.use(this.schema).create(body);

                        this.logger.log('info', `Purging ${ this.cache_key }:all cache`, this.service_name);
                        this.cache.deleteAll(`${ this.cache_key }:all`);

                        this.logger.log('info', `Sending data, ${ document }`, this.service_name);

                        this.socket.emit({ subject: 'Data', data: { type: 'Create', repository: 'Categories', content: document } });

                        return response.status(201).send({
                            status: 'success',
                            message: 'Category created successfully',
                            data: document
                        });
                    } catch (error) {
                        this.logger.log('error', `Saving to database error, ${ error.toString() }`, this.service_name);
                        return next(error);
                    }
                })
                .catch(error => {
                    this.logger.log('error', `Payload validation error, ${ error.toString() }`, this.service_name);
                    const message = this.validator.formatError(error);
                    return next({ message, statusCode: 400 });
                });
        };
    }

    getAllCategory () {
        return (request, response, next) => {
            this.logger.log('info', `getAllCategory()`, this.service_name);

            this.logger.log('info', `Reading cache ${ this.cache_key }:all`, this.service_name);
            return this.cache.read(`${ this.cache_key }:all`)
                .then(async cacheData => {
                    if (cacheData) {
                        this.logger.log('info', `Cache data found, ${ JSON.stringify(cacheData) }`, this.service_name);
                        return response.status(200).send({
                            status: 'success',
                            message: 'Categories retrieved successfully',
                            data: cacheData
                        });
                    }

                    this.logger.log('info', `No cache data found, reading database data`, this.service_name);
                    try {
                        const documents = await this.use(this.schema).fetchAll({});

                        if (documents && documents.length > 0) {
                            this.logger.log('info', `Writing data to cache ${ this.cache_key }:all`, this.service_name);
                            this.cache.write(`${ this.cache_key }:all`, documents);
                        }

                        this.logger.log('info', `Sending data, ${ JSON.stringify(documents) }`, this.service_name);
                        return response.status(200).send({
                            status: 'success',
                            message: 'Categories retrieved successfully',
                            data: documents
                        });
                    } catch (error) {
                        this.logger.log('error', `Reading from database error, ${ error.toString() }`, this.service_name);
                        return  next(error);
                    }
                })
                .catch(error => {
                    this.logger.log('error', `Reading from cache error, ${ error.toString() }`, this.service_name);
                    return next(error);
                });
        };
    }

}

module.exports = Category;
