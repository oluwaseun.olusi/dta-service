"use strict";

const { Router } = require('express');
const {
    product,
    category,
    uploader,
    middleware
} = require('../util/ServiceLocator').bulkInvoke([
    'product',
    'category',
    'uploader',
    'middleware'
]);

const router = Router();

router.route('/products')
    .post(uploader.local_upload().array('images', 12), category.available(), product.duplicate(), middleware.slugify('name'), uploader.cloud_upload(), product.createProduct())
    .get(product.getMeta({ }), product.getAllProduct());

router.route('/products/:_id')
    .get(product.getProduct());

module.exports = router;
