"use strict";

const { Router } = require('express');
const {
    category,
    middleware
} = require('../util/ServiceLocator').bulkInvoke([
    'category',
    'middleware'
]);

const router = Router();

router.route('/categories')
    .post(middleware.slugify('category_name'), category.duplicate(), category.createCategory())
    .get(category.getAllCategory());

module.exports = router;
