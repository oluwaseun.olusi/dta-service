# DTA Service ReadMe

Endpoints:
- **GET** https://dta-service.herokuapp.com/dta-service/api/v1/products
- **GET** https://dta-service.herokuapp.com/dta-service/api/v1/products/:product_id
- **POST** https://dta-service.herokuapp.com/dta-service/api/v1/categories
- **GET** https://dta-service.herokuapp.com/dta-service/api/v1/categories
- **POST** https://dta-service.herokuapp.com/dta-service/api/v1/products


#**GET** https://dta-service.herokuapp.com/dta-service/api/v1/products
This endpoint retrieves the following details of a product
- ID
- name
- price

To test this endpoint, simply send a **GET** request to the URL to retrieve all the products available

#**GET** https://dta-service.herokuapp.com/dta-service/api/v1/products/:product_id
This endpoint retrieves the entire information of a product. TO test this endpoint, simply send a **GET** request to the URL. Make sure to replace **:product_id** with a correct product ID that can be gotten from testing the first endpoint

#**POST** https://dta-service.herokuapp.com/dta-service/api/v1/categories
This endpoint creates a category entry. The required field is
- catregory_name

#**GET** https://dta-service.herokuapp.com/dta-service/api/v1/categories
This endpoint returns all the available categories. This is needed to test the create product endpoint

#**POST** https://dta-service.herokuapp.com/dta-service/api/v1/products
This endpoint creates a new product. The required fields are
- name
- category_id
- description
- price
- tag ---- Array of strings
- color --- Array of color names
- images --- Array of images

Note: The endpoint only processes formdata request

Link to the POSTMAN collection for more detail on response types:
https://documenter.getpostman.com/view/2263874/S11ByMso
