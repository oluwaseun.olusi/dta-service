"use strict";

require('dotenv').config({});

const bodyParser           = require('body-parser');
const config               = require('../src/util/Configuration');
const compression          = require('compression');
const cors                 = require('cors');
const express              = require('express');
const helmet               = require('helmet');
const { database, socket, logger } = require('../src/util/ServiceLocator').bulkInvoke([ 'database', 'socket', 'logger' ]);

const app = express();

app.use(cors());
app.use(helmet());
app.use(compression());

const http = require('http').Server(app);

database.connect();
socket.connect(http);

app.use(bodyParser.urlencoded({ extended: true, limit: '100mb' }));
app.use(bodyParser.json({ limit: '100mb' }));

const router = require('../src/util/Router');

app.use(`${ config.server.base_url }`, router);

http.listen(config.server.port, (error) => {
    if (!error) {
        logger.log('info', `Service is running on port ${ config.server.port }`);
        logger.log('info', `Service base url: ${ config.server.base_url }`);
    } else {
        logger.log('error', error);
    }
});

app.use((error, request, response, next) => {
    logger.log('error', error.message || 'Internal server error, please try again');
    response.status((error.statusCode || 500)).send({
        status: 'error',
        message: (error.message || 'Internal server error, please try again')
    });
});
